# Microservice_Nodejs_Security

una de la serie de Mircroservicio que hace parte de una aplicación realizado en mis tiempos libres

Microservicio de seguridad:

  - ONION DESING PATTERN
  - HEXAGONAL ARCHITECTURE
  - INTERFACES
  - SOLID PRINCIPLES
  
### Installation

 Microservicio de seguridad  [yarn]

Install the dependencies and  start the proyect.
commands:

local
```sh
$ npm install 
$ npm run start
```
production
```sh
$ npm run server:build 
```
fixEslint
```sh
$ npm run format:fix 
```
prettier
```sh
$ npm run format 
```

### ARCHITECTURE Microservice_Nodejs_Security

![](imagenPrograma/MICROSERVICIOS%20EN%20NODEJS.png)


### Library

this project has the following pluglins implemented

| Plugin |
| ------ |
| sequelize.js | [plugins/dropbox/README.md][PlDb] |
| jwt-simple.js | [plugins/dropbox/README.md][PlDb] |
| express.js | [plugins/dropbox/README.md][PlDb] |
| gulp | [plugins/dropbox/README.md][PlDb] |



<img src="https://github.com/favicon.ico" width="48">
<img src="https://pnghut.com/png/ipkaeNhYHs/node-js-javascript-software-developer-angularjs-nodejs-javascript-library-transparent-png" width="48">

