"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthUseCase = void 0;
const role_token_dto_1 = require("./role-token.dto");
const auth_token_operation_1 = require("../infraestructure/auth-token.operation");
class AuthUseCase {
    constructor(authRepository) {
        this.authRepository = authRepository;
    }
    async login(user) {
        const response = await this.authRepository.login(user);
        if (response) {
            const { email, lastname, username, name, roles } = response;
            const userMatched = { email, lastname, username, name, roles };
            const roleDto = role_token_dto_1.RoleTokenDto(roles);
            userMatched.roles = roleDto;
            return {
                accessToken: auth_token_operation_1.Token.generateAccessToken(userMatched),
            };
        }
        return {
            error: 'Usuario no encontrado',
            status: 404,
        };
    }
}
exports.AuthUseCase = AuthUseCase;
