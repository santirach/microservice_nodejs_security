"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleTokenDto = void 0;
const RoleTokenDto = (roles) => {
    const rolesDto = roles.map(({ name, id }) => {
        return { name, id };
    });
    return rolesDto;
};
exports.RoleTokenDto = RoleTokenDto;
