"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
class AuthController {
    constructor(userUseCase) {
        this.userUseCase = userUseCase;
    }
    async login(user) {
        return this.userUseCase.login(user);
    }
}
exports.AuthController = AuthController;
