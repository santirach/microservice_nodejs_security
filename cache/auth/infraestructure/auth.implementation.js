"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const auth_operation_1 = require("./auth.operation");
const auth_usecase_1 = require("../application/auth.usecase");
const auth_controller_1 = require("./auth.controller");
const authOperation = new auth_operation_1.AuthOperation();
const authUseCase = new auth_usecase_1.AuthUseCase(authOperation);
const authController = new auth_controller_1.AuthController(authUseCase);
exports.default = authController;
