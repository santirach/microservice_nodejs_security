"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthOperation = void 0;
const user_model_1 = require("../../user/infraestructure/user.model");
const role_model_1 = require("../../role/infraestructure/role.model");
class AuthOperation {
    async login({ email }) {
        try {
            const response = (await user_model_1.UserModel.findOne({ where: { email }, include: [role_model_1.Role] }));
            return response;
        }
        catch (error) {
            console.log('error consultando:::>', error);
        }
    }
}
exports.AuthOperation = AuthOperation;
