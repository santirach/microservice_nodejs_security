"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const express_1 = __importDefault(require("express"));
const user_implementation_1 = __importDefault(require("../../user/infraestructure/user.implementation"));
const role_implementation_1 = __importDefault(require("../../role/infraestructure/role.implementation"));
const userRole_implementation_1 = __importDefault(require("../../user_role/infraestructure/userRole.implementation"));
const auth_implementation_1 = __importDefault(require("./auth.implementation"));
const router = express_1.default.Router();
exports.router = router;
router.get('/users', async (req, res) => {
    const result = await user_implementation_1.default.getAll({});
    res.json(result);
});
router.get('/roles', async (req, res) => {
    const result = await role_implementation_1.default.getAll({});
    res.json(result);
});
router.get('/user_role', async (req, res) => {
    const result = await userRole_implementation_1.default.getAll({});
    res.json(result);
});
router.post('/', async (req, res) => {
    const { email, password } = req.body;
    const user = {
        email,
        password,
    };
    const result = await auth_implementation_1.default.login(user);
    if (result) {
        return res.json(result);
    }
    res.status(404).json({ error: 'no found' });
});
