"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql_1 = __importDefault(require("mysql"));
class DatabaseMysql {
    constructor() {
        this.connection = mysql_1.default.createConnection({
            host: 'localhost',
            user: 'root',
            password: 'Aforo255#2019',
            database: 'db_security',
            port: 3308,
        });
    }
    initialize() {
        const promiseInitialize = new Promise((resolve, reject) => {
            const callback = (error) => {
                if (error) {
                    console.log('error conetion database mysql');
                    reject(error);
                }
                else {
                    console.log('Connection Database successful');
                    resolve('Connection Database successful');
                }
            };
            this.connection.connect(callback);
        });
        return promiseInitialize;
    }
    disconnect() {
        try {
            this.connection.end();
            this.connection.destroy();
        }
        catch (error) {
            console.log(error);
        }
    }
}
exports.default = DatabaseMysql;
