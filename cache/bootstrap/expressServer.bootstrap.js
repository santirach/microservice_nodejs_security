"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const auth_routes_1 = require("../auth/infraestructure/auth.routes");
const multer_1 = __importDefault(require("multer"));
const app = express_1.default();
const upload = multer_1.default();
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use('/auth', upload.fields([]), auth_routes_1.router);
exports.default = app;
