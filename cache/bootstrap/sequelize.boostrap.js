"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const yenv_1 = __importDefault(require("yenv"));
const env = yenv_1.default();
class DatabaseSequelize {
    constructor() {
        if (DatabaseSequelize.instance) {
            return DatabaseSequelize.instance;
        }
        this.connection = new sequelize_1.Sequelize(env.DATABASE.MYSQL.DB, env.DATABASE.MYSQL.USER, env.DATABASE.MYSQL.PASS, {
            host: env.DATABASE.MYSQL.HOST,
            port: env.DATABASE.MYSQL.PORT,
            dialect: env.DATABASE.MYSQL.DIALECT,
        });
        DatabaseSequelize.instance = this;
    }
    initialize() {
        const promiseInitialize = new Promise((resolve, reject) => {
            this.connection
                .authenticate()
                .then(() => {
                console.log('Connection Database successful');
                resolve('Connection Database successful');
            })
                .catch(error => {
                console.log('error conetion database mysql');
                reject(error);
            });
        });
        return promiseInitialize;
    }
    disconnect() {
        return this.connection.close();
    }
    connetion() {
        return this.connection;
    }
}
exports.default = DatabaseSequelize;
