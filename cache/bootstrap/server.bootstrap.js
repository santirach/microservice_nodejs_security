"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const yenv_1 = __importDefault(require("yenv"));
const env = yenv_1.default();
class Server {
    constructor(aplication) {
        this.aplication = aplication;
    }
    async initialize() {
        const serverPromise = new Promise((resolve, reject) => {
            const server = http_1.default.createServer(this.aplication);
            server
                .listen(env.PORT)
                .on('listening', () => {
                console.log('Server is runing');
                resolve('runing');
            })
                .on('error', error => {
                console.log('error al runing server');
                reject(error);
            });
        });
        return await serverPromise;
    }
}
exports.default = Server;
