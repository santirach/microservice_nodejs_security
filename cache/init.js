"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_bootstrap_1 = __importDefault(require("./bootstrap/server.bootstrap"));
const expressServer_bootstrap_1 = __importDefault(require("./bootstrap/expressServer.bootstrap"));
const sequelize_boostrap_1 = __importDefault(require("./bootstrap/sequelize.boostrap"));
const start = async () => {
    const server = new server_bootstrap_1.default(expressServer_bootstrap_1.default);
    const databaseMysql = new sequelize_boostrap_1.default();
    try {
        await server.initialize();
        await databaseMysql.initialize();
    }
    catch (error) {
        console.log(error);
        databaseMysql.disconnect();
    }
    /* const serverRes = await server.initialize().catch((error)=>{
    console.log('error al correr el servidor',error)
    })
    console.log('::::::::::::::::::::>', serverRes) */
};
start();
