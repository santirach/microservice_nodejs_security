"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RoleUseCase {
    constructor(repository) {
        this.repository = repository;
    }
    async getAll(filters) {
        const results = await this.repository.getAll(filters);
        return results;
    }
}
exports.default = RoleUseCase;
