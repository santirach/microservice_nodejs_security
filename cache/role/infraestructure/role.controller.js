"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RoleController {
    constructor(roleUseCase) {
        this.roleUseCase = roleUseCase;
    }
    async getAll(filters) {
        return this.roleUseCase.getAll(filters);
    }
}
exports.default = RoleController;
