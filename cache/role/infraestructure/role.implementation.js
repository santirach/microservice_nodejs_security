"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const role_operation_1 = __importDefault(require("./role.operation"));
const role_usecase_1 = __importDefault(require("../application/role.usecase"));
const role_controller_1 = __importDefault(require("./role.controller"));
const roleOperation = new role_operation_1.default();
const roleUseCase = new role_usecase_1.default(roleOperation);
const roleImplementation = new role_controller_1.default(roleUseCase);
exports.default = roleImplementation;
