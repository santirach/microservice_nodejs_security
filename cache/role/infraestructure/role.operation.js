"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const role_model_1 = require("./role.model");
class RoleOperation {
    constructor() {
        this.model = role_model_1.Role;
    }
    async getAll(filter) {
        const items = await this.model.findAll(filter);
        return items;
    }
}
exports.default = RoleOperation;
