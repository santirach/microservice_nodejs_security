"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUseCase = void 0;
class UserUseCase {
    constructor(repository) {
        this.repository = repository;
    }
    async getAll(filters) {
        const results = await this.repository.getAll(filters);
        return results;
    }
}
exports.UserUseCase = UserUseCase;
