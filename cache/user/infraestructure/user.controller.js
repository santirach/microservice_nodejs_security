"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
class UserController {
    constructor(userUseCase) {
        this.userUseCase = userUseCase;
    }
    async getAll(filters) {
        return this.userUseCase.getAll(filters);
    }
}
exports.UserController = UserController;
