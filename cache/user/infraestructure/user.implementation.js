"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_operation_1 = require("./user.operation");
const user_usecase_1 = require("../application/user.usecase");
const user_controller_1 = require("./user.controller");
const userOperation = new user_operation_1.UserOperation();
const userUseCase = new user_usecase_1.UserUseCase(userOperation);
const userImplementation = new user_controller_1.UserController(userUseCase);
exports.default = userImplementation;
