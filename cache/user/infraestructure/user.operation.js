"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserOperation = void 0;
const user_model_1 = require("./user.model");
class UserOperation {
    constructor() {
        this.model = user_model_1.UserModel;
    }
    async getAll(filter) {
        /* this.model.findAll({})
            .then((nodes: Array<any>) => res.json(nodes))
            .catch((err: Error) => res.status(500).json(err)); */
        const items = await this.model.findAll(filter);
        return items;
    }
}
exports.UserOperation = UserOperation;
