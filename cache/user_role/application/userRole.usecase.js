"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserRoleUseCase {
    constructor(userRoleOperation) {
        this.userRoleOperation = userRoleOperation;
    }
    async getAll(filter) {
        const result = await this.userRoleOperation.getAll(filter);
        return result;
    }
}
exports.default = UserRoleUseCase;
