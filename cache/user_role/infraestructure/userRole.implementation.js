"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const userRole_operation_1 = require("./userRole.operation");
const userRole_usecase_1 = __importDefault(require("../application/userRole.usecase"));
const userRole_controller_1 = __importDefault(require("./userRole.controller"));
const userRoleOperation = new userRole_operation_1.UserRoleOperation();
const userRoleUseCase = new userRole_usecase_1.default(userRoleOperation);
const userRoleImplementation = new userRole_controller_1.default(userRoleUseCase);
exports.default = userRoleImplementation;
