"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRole = void 0;
const Sequelize = __importStar(require("sequelize"));
const sequelize_boostrap_1 = __importDefault(require("../../bootstrap/sequelize.boostrap"));
const user_model_1 = require("../../user/infraestructure/user.model");
const role_model_1 = require("../../role/infraestructure/role.model");
const connection = new sequelize_boostrap_1.default().connetion();
/* export interface UserViewModel {
    id: number
    email: string
} */
exports.UserRole = connection.define('user_role', {
    user_id: Sequelize.NUMBER,
    role_id: Sequelize.NUMBER,
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_role',
});
user_model_1.UserModel.belongsToMany(role_model_1.Role, { through: exports.UserRole, foreignKey: 'user_id' });
role_model_1.Role.belongsToMany(user_model_1.UserModel, { through: exports.UserRole, foreignKey: 'role_id' });
