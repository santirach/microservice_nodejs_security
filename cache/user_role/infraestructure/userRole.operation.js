"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoleOperation = void 0;
const userRole_model_1 = require("./userRole.model");
class UserRoleOperation {
    constructor() {
        this.model = userRole_model_1.UserRole;
    }
    async getAll(filter) {
        const items = await this.model.findAll(filter);
        return items;
    }
}
exports.UserRoleOperation = UserRoleOperation;
