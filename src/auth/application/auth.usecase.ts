import { AuthRepository } from '../domain/repositories/auth.repository';
import { User } from '../../user/domain/entities/user.entities';
import { RoleTokenDto } from './role-token.dto';
import { Token } from '../infraestructure/auth-token.operation';

export class AuthUseCase {
	constructor(private readonly authRepository: AuthRepository) {}

	async login(user: User) {
		const response: User = await this.authRepository.login(user);
		if (response) {
			const { email, lastname, username, name, roles } = response;
			const userMatched: User = { email, lastname, username, name, roles };
			const roleDto = RoleTokenDto(roles);
			userMatched.roles = roleDto;
			return {
				accessToken: Token.generateAccessToken(userMatched),
			};
		}
		return {
			error: 'Usuario no encontrado',
			status: 404,
		};
	}
}
