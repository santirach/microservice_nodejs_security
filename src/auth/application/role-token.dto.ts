const RoleTokenDto = (roles: any[]): any[] => {
	const rolesDto = roles.map(({ name, id }) => {
		return { name, id };
	});

	return rolesDto;
};

export { RoleTokenDto };
