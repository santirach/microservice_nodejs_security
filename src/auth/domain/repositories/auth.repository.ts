import { User } from '../../../user/domain/entities/user.entities';

export interface AuthRepository {
	login(user: User): Promise<User>;
}
