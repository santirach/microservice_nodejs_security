import { AuthUseCase } from '../application/auth.usecase';
import { User } from '../../user/domain/entities/user.entities';

export class AuthController {
	constructor(private readonly userUseCase: AuthUseCase) {}
	async login(user: User) {
		return this.userUseCase.login(user);
	}
}
