import { AuthOperation } from './auth.operation';
import { AuthUseCase } from '../application/auth.usecase';
import { AuthController } from './auth.controller';

const authOperation = new AuthOperation();
const authUseCase = new AuthUseCase(authOperation);
const authController = new AuthController(authUseCase);

export default authController;
