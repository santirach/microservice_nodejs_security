import { User } from '../../user/domain/entities/user.entities';
import { AuthRepository } from '../domain/repositories/auth.repository';
import { UserModel } from '../../user/infraestructure/user.model';
import { Role } from '../../role/infraestructure/role.model';

export class AuthOperation implements AuthRepository {
	async login({ email }: User): Promise<User> {
		try {
			const response: User = <User>(
				await UserModel.findOne({ where: { email }, include: [Role] })
			);
			return response;
		} catch (error) {
			console.log('error consultando:::>', error);
		}
	}
}
