import express from 'express';
import userImplementation from '../../user/infraestructure/user.implementation';
import roleImplementation from '../../role/infraestructure/role.implementation';
import userRoleImplementation from '../../user_role/infraestructure/userRole.implementation';
import authController from './auth.implementation';
import { User } from '../../user/domain/entities/user.entities';

const router = express.Router();

router.get('/users', async (req, res) => {
	const result = await userImplementation.getAll({});
	res.json(result);
});

router.get('/roles', async (req, res) => {
	const result = await roleImplementation.getAll({});
	res.json(result);
});

router.get('/user_role', async (req, res) => {
	const result = await userRoleImplementation.getAll({});
	res.json(result);
});

router.post('/', async (req, res) => {
	const { email, password } = req.body;
	const user: User = {
		email,
		password,
	};
	const result = await authController.login(user);
	if (result) {
		return res.json(result);
	}
	res.status(404).json({ error: 'no found' });
});

export { router };
