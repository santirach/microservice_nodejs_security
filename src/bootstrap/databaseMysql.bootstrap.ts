import mysql from 'mysql';
import { DatabaseRepository } from '../interfaces/databaseRepository.interfaces';

export default class DatabaseMysql implements DatabaseRepository {
	private connection: any;
	constructor() {
		this.connection = mysql.createConnection({
			host: 'localhost',
			user: 'root',
			password: 'Aforo255#2019',
			database: 'db_security',
			port: 3308,
		});
	}

	initialize(): Promise<any> {
		const promiseInitialize = new Promise((resolve, reject) => {
			const callback = (error: any) => {
				if (error) {
					console.log('error conetion database mysql');
					reject(error);
				} else {
					console.log('Connection Database successful');
					resolve('Connection Database successful');
				}
			};

			this.connection.connect(callback);
		});

		return promiseInitialize;
	}

	disconnect(): void {
		try {
			this.connection.end();
			this.connection.destroy();
		} catch (error) {
			console.log(error);
		}
	}
}
