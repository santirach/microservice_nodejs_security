import express from 'express';
import bodyParser from 'body-parser';
import { router as routerAuth } from '../auth/infraestructure/auth.routes';
import multer, { Multer } from 'multer';

const app = express();
const upload: Multer = multer();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/auth', upload.fields([]), routerAuth);

export default app;
