import { Sequelize } from 'sequelize';
import { DatabaseRepository } from '../interfaces/databaseRepository.interfaces';
import yenv from 'yenv';
const env = yenv();
export default class DatabaseSequelize implements DatabaseRepository {
	private static instance: DatabaseSequelize;
	private connection: Sequelize;
	constructor() {
		if (DatabaseSequelize.instance) {
			return DatabaseSequelize.instance;
		}
		this.connection = new Sequelize(
			env.DATABASE.MYSQL.DB,
			env.DATABASE.MYSQL.USER,
			env.DATABASE.MYSQL.PASS,
			{
				host: env.DATABASE.MYSQL.HOST,
				port: env.DATABASE.MYSQL.PORT,
				dialect: env.DATABASE.MYSQL.DIALECT,
			}
		);
		DatabaseSequelize.instance = this;
	}

	initialize(): Promise<any> {
		const promiseInitialize = new Promise((resolve, reject) => {
			this.connection
				.authenticate()
				.then(() => {
					console.log('Connection Database successful');
					resolve('Connection Database successful');
				})
				.catch(error => {
					console.log('error conetion database mysql');
					reject(error);
				});
		});
		return promiseInitialize;
	}

	disconnect(): Promise<any> {
		return this.connection.close();
	}

	connetion() {
		return this.connection;
	}
}
