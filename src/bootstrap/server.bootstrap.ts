import http from 'http';
import yenv from 'yenv';
const env = yenv();

export default class Server {
	constructor(private aplication: any) {}

	async initialize() {
		const serverPromise = new Promise((resolve, reject) => {
			const server: http.Server = http.createServer(this.aplication);

			server
				.listen(env.PORT)
				.on('listening', () => {
					console.log('Server is runing');
					resolve('runing');
				})
				.on('error', error => {
					console.log('error al runing server');
					reject(error);
				});
		});

		return await serverPromise;
	}
}
