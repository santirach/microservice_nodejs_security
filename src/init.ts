import Server from './bootstrap/server.bootstrap';
import app from './bootstrap/expressServer.bootstrap';
import DatabaseSequelize from './bootstrap/sequelize.boostrap';

const start = async () => {
	const server = new Server(app);
	const databaseMysql = new DatabaseSequelize();

	try {
		await server.initialize();
		await databaseMysql.initialize();
	} catch (error) {
		console.log(error);
		databaseMysql.disconnect();
	}
	/* const serverRes = await server.initialize().catch((error)=>{
    console.log('error al correr el servidor',error)
    })    
    console.log('::::::::::::::::::::>', serverRes) */
};

start();
