import { RoleRepository } from '../domain/repository/role.repository';

export default class RoleUseCase {
	constructor(private readonly repository: RoleRepository) {}

	async getAll(filters: any) {
		const results = await this.repository.getAll(filters);

		return results;
	}
}
