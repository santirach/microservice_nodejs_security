export interface RoleRepository {
	getAll(filter: any): Promise<Array<any>>;
}
