import RoleUseCase from '../application/role.usecase';

export default class RoleController {
	constructor(private readonly roleUseCase: RoleUseCase) {}

	async getAll(filters: any) {
		return this.roleUseCase.getAll(filters);
	}
}
