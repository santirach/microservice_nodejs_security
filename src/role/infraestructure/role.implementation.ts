import RoleOperation from './role.operation';
import RoleUseCase from '../application/role.usecase';
import RoleController from './role.controller';

const roleOperation = new RoleOperation();
const roleUseCase = new RoleUseCase(roleOperation);
const roleImplementation = new RoleController(roleUseCase);

export default roleImplementation;
