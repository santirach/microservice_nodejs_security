import * as Sequelize from 'sequelize';
import DatabaseSequelize from '../../bootstrap/sequelize.boostrap';

const connection = new DatabaseSequelize().connetion();

export interface RoleAddModel {
	name: string;
}

export interface RoleModel extends Sequelize.Model<RoleModel, RoleAddModel> {
	id: number;
}

/* export interface UserViewModel {
    id: number
    email: string
} */

export const Role = connection.define<RoleModel, RoleAddModel>(
	'role',
	{
		name: Sequelize.STRING,
	},
	{
		timestamps: false,
		freezeTableName: true,
		tableName: 'role',
	}
);
