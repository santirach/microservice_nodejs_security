import { RoleRepository } from '../domain/repository/role.repository';
import { Role as RoleModel } from './role.model';
export default class RoleOperation implements RoleRepository {
	private model: typeof RoleModel;

	constructor() {
		this.model = RoleModel;
	}

	async getAll(filter: any): Promise<any[]> {
		const items: any[] = await this.model.findAll(filter);
		return items;
	}
}
