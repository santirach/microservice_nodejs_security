import { UserRepository } from '../domain/repository/user.repository';

export class UserUseCase {
	constructor(private readonly repository: UserRepository) {}

	async getAll(filters: any) {
		const results = await this.repository.getAll(filters);

		return results;
	}
}
