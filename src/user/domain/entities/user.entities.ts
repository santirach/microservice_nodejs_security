export interface User {
	email?: string;
	password?: string;
	name?: string;
	lastname?: string;
	username?: string;
	roles?: any;
}
