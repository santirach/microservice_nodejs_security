import { User } from '../entities/user.entities';

export interface UserRepository {
	getAll(filter: any): Promise<Array<User>>;
}
