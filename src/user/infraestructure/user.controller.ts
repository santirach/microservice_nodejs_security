import { UserUseCase } from '../application/user.usecase';

export class UserController {
	constructor(private readonly userUseCase: UserUseCase) {}
	async getAll(filters: any) {
		return this.userUseCase.getAll(filters);
	}
}
