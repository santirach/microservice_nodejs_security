import { UserOperation } from './user.operation';
import { UserUseCase } from '../application/user.usecase';
import { UserController } from './user.controller';

const userOperation = new UserOperation();
const userUseCase = new UserUseCase(userOperation);
const userImplementation = new UserController(userUseCase);

export default userImplementation;
