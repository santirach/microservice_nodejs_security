import * as Sequelize from 'sequelize';
import DatabaseSequelize from '../../bootstrap/sequelize.boostrap';
import { User } from '../domain/entities/user.entities';

const connection = new DatabaseSequelize().connetion();

export interface UserDefinition extends Sequelize.Model<User> {
	id: number;
}

export const UserModel = connection.define<UserDefinition, User>(
	'user',
	{
		email: Sequelize.STRING,
		password: Sequelize.STRING,
		name: Sequelize.STRING,
		lastname: Sequelize.STRING,
		username: Sequelize.STRING,
	},
	{
		timestamps: false,
		freezeTableName: true,
		tableName: 'user',
	}
);
