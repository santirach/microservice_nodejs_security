import { UserRepository } from '../domain/repository/user.repository';
import { User } from '../domain/entities/user.entities';
import { UserModel } from './user.model';

export class UserOperation implements UserRepository {
	private model: typeof UserModel;

	constructor() {
		this.model = UserModel;
	}

	async getAll(filter: any): Promise<User[]> {
		/* this.model.findAll({})
            .then((nodes: Array<any>) => res.json(nodes))
            .catch((err: Error) => res.status(500).json(err)); */
		const items: any[] = await this.model.findAll(filter);
		return items;
	}
}
