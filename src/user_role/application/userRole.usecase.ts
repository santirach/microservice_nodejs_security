import { UserRoleRepository } from '../domain/repository/userRole.repository';

export default class UserRoleUseCase {
	constructor(private readonly userRoleOperation: UserRoleRepository) {}

	async getAll(filter: any): Promise<any[]> {
		const result = await this.userRoleOperation.getAll(filter);
		return result;
	}
}
