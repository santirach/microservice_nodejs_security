export interface UserRoleRepository {
	getAll(filter: any): Promise<Array<any>>;
}
