import UserRoleUseCase from '../application/userRole.usecase';
export default class UserRoleController {
	constructor(private readonly userRoleUseCase: UserRoleUseCase) {}

	async getAll(filters: any) {
		return this.userRoleUseCase.getAll(filters);
	}
}
