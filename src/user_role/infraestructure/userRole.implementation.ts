import { UserRoleOperation } from './userRole.operation';
import UserRoleUseCase from '../application/userRole.usecase';
import UserRoleController from './userRole.controller';

const userRoleOperation = new UserRoleOperation();
const userRoleUseCase = new UserRoleUseCase(userRoleOperation);
const userRoleImplementation = new UserRoleController(userRoleUseCase);

export default userRoleImplementation;
