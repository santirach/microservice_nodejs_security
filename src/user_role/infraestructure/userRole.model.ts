import * as Sequelize from 'sequelize';
import DatabaseSequelize from '../../bootstrap/sequelize.boostrap';
import { UserModel } from '../../user/infraestructure/user.model';
import { Role } from '../../role/infraestructure/role.model';

const connection = new DatabaseSequelize().connetion();

export interface UserRoleAddModel {
	user_id: number;
	role_id: number;
}

export interface UserRoleModel
	extends Sequelize.Model<UserRoleModel, UserRoleAddModel> {
	id: number;
}

/* export interface UserViewModel {
    id: number
    email: string
} */

export const UserRole = connection.define<UserRoleModel, UserRoleAddModel>(
	'user_role',
	{
		user_id: Sequelize.NUMBER,
		role_id: Sequelize.NUMBER,
	},
	{
		timestamps: false,
		freezeTableName: true,
		tableName: 'user_role',
	}
);

UserModel.belongsToMany(Role, { through: UserRole, foreignKey: 'user_id' });
Role.belongsToMany(UserModel, { through: UserRole, foreignKey: 'role_id' });
