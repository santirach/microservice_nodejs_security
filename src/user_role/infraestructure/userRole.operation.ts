import { UserRoleRepository } from '../domain/repository/userRole.repository';
import { UserRole as UserRoleModel } from './userRole.model';

export class UserRoleOperation implements UserRoleRepository {
	private model: typeof UserRoleModel;

	constructor() {
		this.model = UserRoleModel;
	}

	async getAll(filter: any): Promise<any[]> {
		const items: any[] = await this.model.findAll(filter);
		return items;
	}
}
