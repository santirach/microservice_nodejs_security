import { UserOperation } from "../../src/user/infraestructure/user.operation"
import { UserUseCase } from "../../src/user/application/user.usecase";
import userMock from '../mocks/medic.mock.json';

let userOperation;
let userUseCase: any;
describe('user.usecase', () => {
    beforeAll(() => {
        (UserOperation as jest.Mock) = jest.fn().mockReturnValue({

            getAll: jest.fn().mockResolvedValue(userMock),

        });

        userOperation = new UserOperation();
        userUseCase = new UserUseCase(userOperation);
    });
    it('getAll', async () => {
        const result = await userUseCase.getAll(true);
        expect(result).toEqual(userMock);
    })
})